postgres:
	docker run --name postgres -p 5432:5432 -e POSTGRES_PASSWORD=1234 -e POSTGRES_USER=postgres -d postgres

createdb:
	docker exec -it postgres createdb --username=postgres --owner=postgres simple_bank

migrateup:
	migrate -path db/migration -database postgresql://postgres:1234@localhost:5432/simple_bank?sslmode=disable -verbose up

migratedown:
	migrate -path db/migration -database postgresql://root:1234@localhost:5432/simple_bank?sslmode=disable -verbose down

sqlc:
	sqlc generate

test:
	go test -v -cover ./...

server:
	go run main.go

.PHONY: network postgres createdb dropdb migrateup migratedown test sqlc server