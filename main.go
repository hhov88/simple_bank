package main

import (
	"database/sql"
	"github.com/hhov88/simplebank/api"
	db "github.com/hhov88/simplebank/db/postgresql"
	"github.com/hhov88/simplebank/utils"
	_ "github.com/lib/pq" // <------------ here
	"log"
)

func main() {
	config, err := utils.LoadConfig(".")
	if err != nil {
		log.Fatal("config not found: ", err)
	}

	conn, err := sql.Open(config.DBDriver, config.DBSource)

	if err != nil {
		log.Fatal("cannot connect to the Db: ", err)
	}

	store := db.NewStore(conn)
	server := api.NewServer(store)

	if err := server.Start(config.SourceAddress); err != nil {
		log.Fatal("cannot start server: ", err)
	}
}
