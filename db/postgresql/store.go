package db

import (
	"context"
	"database/sql"
	"fmt"
)

type Store struct {
	*Queries
	db *sql.DB
}

type TranferTxParams struct {
	FromAccountID int64 `json:"from_account_id"`
	ToAccountID   int64 `json:"to_account_id"`
	Amount        int64 `json:"amount"`
}

type TranferTxResult struct {
	Transfer    Transfer `json:"transfer"`
	FromAccount Account  `json:"from_account"`
	ToAccount   Account  `json:"to_account"`
	FromEntry   Account  `json:"from_entry"`
	ToEntry     Account  `json:"to_entry"`
}

func NewStore(db *sql.DB) *Store {
	return &Store{
		db:      db,
		Queries: New(db),
	}
}

func (store *Store) execTx(ctx context.Context, fn func(*Queries) error) error {
	tx, err := store.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	} else {
		q := New(tx)
		err = fn(q)
		if err != nil {
			if rlberr := tx.Rollback(); rlberr != nil {
				return fmt.Errorf("tx err: %v, rb err: %v ", err, rlberr)
			} else {
				return err
			}
		}
		return tx.Commit()
	}
}

// func (store *Store) TransferTx(ctx context.Context, arg TranferTxParams) (TranferTxResult, error) {
// 	var result TranferTxResult
// 	err := store.execTx(ctx, func(q *Queries) error {
// 		var err error
// 		result.Transfer, err := q.CreateTranfer(ctx, TranferTxParams{
// 			FromAccountID: arg.FromAccountID,
// 			ToAccountID:   arg.ToAccountID,
// 			Amount:        arg.Amount,
// 		})
// 		if err != nil {
// 			return err
// 		}

// 		result.Transfer, err := q.CreateAccount(ctx, TranferTxParams{
// 			FromAccountID: arg.FromAccountID,
// 			ToAccountID:   arg.ToAccountID,
// 			Amount:        arg.Amount,
// 		})
// 		if err != nil {
// 			return err
// 		}
// 	})
// 	return result, err
// }
